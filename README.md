# Wokwi Pi Pico Worksheet

Worksheet for a gentle introduction to the Raspberry Pi Pico using the Wokwi virtual Pico.

All work is carried out in the browser at: https://wokwi.com/ and will not require a physical Pi Pico to complete....well that's if don't want to try them in real life :)

Content is created using asciidoc: https://asciidoctor.org/

This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License