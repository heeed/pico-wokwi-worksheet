= Wokwi Pi Pico Workshop
:source-highlighter: rouge
:version-label: 1 (July 2022)
:description: Except for third party links, this work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Twitter: @heeedt email: heeedt@proton.me

Hello 

Welcome to this worksheet about the Raspberry Pi Pico.

By the end of the session you will know how to write some simple Python code to control a LED.

== But first....what is a Pi Pico?

A Pi Pico is a small microcontroller based board. A microcontroller is similar to the computer your reading this file on but, unlike the computer, can only do one task.

The board is based upon the RP2040 chip which has been developed by Raspberry Pi. Further details can be found here: https://www.raspberrypi.com/documentation/microcontrollers/rp2040.html


.A Pi Pico
[#img-pico]
image::./images/pico.png[]

It has the following specifications:

•	21 mm × 51 mm form factor
•	RP2040 microcontroller chip designed by Raspberry Pi in the UK
•	Dual-core Arm Cortex-M0+ processor, flexible clock running up to 133 MHz
•	264KB on-chip SRAM
•	2MB on-board QSPI Flash
•	26 multifunction GPIO pins, including 3 analogue inputs
•	2 × UART, 2 × SPI controllers, 2 × I2C controllers, 16 × PWM channels
•	1 × USB 1.1 controller and PHY, with host and device support
•	8 × Programmable I/O (PIO) state machines for custom peripheral support

Each board costs around £4

<<<

Recently, a new version has been released which includes on board WiFi but is otherwise identical in operation.

.A Pi Pico Wireless
[#img-picow]
image::./images/pico_w.png[200,200]

This version costs around £6

Both have the following Pinout:

[#img-pinout]
.link:https://datasheets.raspberrypi.com/pico/Pico-R3-A4-Pinout.pdf[Raspberry Pi Pico Pinout] 
image::images/pinout.png["Pi Pico Pinout"]

<<<

== Lets Program one!

Great...you are still here :)

So, as part of the session, we are going to write some code to display some text and then, hopefully, build a simple traffic light system. 

To start with we are going to use an online browser based emulator called Wokwi and once we have been succesful we will then try it on a real Pi Pico. We are going to use the microPython language but there are other languages availible...that can be something to explore later.

=== Wokwi

Wokwi allows you to tinker with various small microcontrollers and some basic electronic components. It can be found here: https://wokwi.com/. If you click on this link you will find the site shown below:

.Wokwi Home Page
[#img-wokwi]
image::./images/wokwi1.png[300,300]

Next, as we will be using MicroPython, click on the _Micropython on Pi Pico_ link.

.MicroPython Link
[#img-wokwi]
image::./images/wokwi2.png[300,300]

<<<

This should then open something similar to this:

.Wokwi Pi Pico 
[#img-wokwi]
image::./images/wokwi3.png[500,500]

To start with let's click the run button and see what happens. (It's the green circle with the triangle inside it)

When we click the circle the following happens:

.Wokwi Output 
[#img-wokwi]
image::./images/wokwi5.png[500,500]

The simulator starts and an output box appears under the Pi Pico. If you look closely you should notice that it is saying hello to you. This is because the code in the left hand panel has told it to _print_ the message.

[source, python]
----
print("Hello, Pi Pico")
----

So, what do you think will happen if you change the message?

****
Challenge:

* Can the words be changed?
* Can you get more lines of text to display in the output?

(Don't forget to click the green run button after each change)

****

=== Flashing a LED

OK, so we now have the print function under control...let's see if we can flash a LED next.

A LED is whats known as a _L_ ight _E_ mmiting _D_ iode. At this level think of it like a light bulb that you have at home. Power is applied and light comes out. If your curious then more info can be found https://electronicsclub.info/leds.htm[here]

The Pi Pico has a built in LED that is accessible via GP25:

[#img-virtual-led]
.GP25 LED
image::./images/LED.png["GP25 LED",400,400]


[NOTE]
====
So what is a GP25 when it's at home?

The Pi Pico has 26 multi-function GPIO pins supplied by the RP2040 chip. GPIO means that they can be used for *General Purpose Input Output* activities once they have been suitably configured.

GP25 just means that this is the 25th GPIO connection availiable from the RP2040. It has been dedicated to allowing access to that one LED.
====

Right...time to flash it on and off.

Firstly, clear any exisiting code from Wokwi:

[#img-virtual-emptycode]
.All code cleared
image::images/cleared.png["Cleared Code"] 

Now carefully copy the code below:

Blinking Led.py footnote:[https://hackspace.raspberrypi.com/books/micropython-pico[Getting started with MicroPython on Raspberry Pi Picp] Page 47]


[source%linenums,python]
------
import machine
import utime
led_onboard = machine.Pin(25, machine.Pin.OUT)
while True:
    led_onboard.value(1)
    utime.sleep(5)
    led_onboard.value(0)
    utime.sleep(5)
------
.footnote[https://hackspace.raspberrypi.com/books/micropython-pico[Getting started with MicroPython on Raspberry Pi Picp] Page 47]


[#img-virtual-code]
.Blinking code in Wokwi
image::images/virt_code.png["Blinking Code"]

[NOTE]
====
Lets quickly explore what this code is doing:

[source%linenums,python]
------
import machine
import utime
------
These two lines are used to _import_ the following two modules: _machine_ and _utime_. These are only needed once in our code and must be placed on the first two lines.

[source%linenums,python]
----
led_onboard = machine.Pin(25, machine.Pin.OUT)
----

This line configures the pin connected to GP25 to act an output. It then creates a connection to this pin called _led_onboard_ to allow our code to use it. We can have multiple connections to pins set up here but each must have a unique name.

[source%linenums,python]
------
while True:
    led_onboard.value(1)
    utime.sleep(5)
    led_onboard.value(0)
    utime.sleep(5)
------

This final block of code is where the magic happens. It uses a a _while True:_ to constantly loop aound the lines after. 
In this case:

* _led_onboard_ is turned on.
* The device id put to sleep by the _utime.sleep()_ for five seconds.
* _led_onboard_ is turned off.
* The device id put to sleep by the _utime.sleep()_ for five seconds.
* _led_onboard_ is turned on....and the loop continues

We can add extra commands here, if needed, to extend what our code does.

https://peps.python.org/pep-0008/#indentation[*Just remember to start the line with four spaces*]. 

====

Once done click the green run button and the Virtual LED should flash at 5 second intervals.

****
Challenge:

* Can the timing of the flashes be changed? 

For example: Turn on for 5 seconds and then off for 2?

****

'''

=== Project Traffic Lights

So far we have persuaded the Pi Pico to print text and also turned the GP25 LED on and off.

Now we are going to see if we can build a traffic light system. However, firstly, we need to learn how to wire up an extenal LED in Wokwi. 

In the simulation pane, click on the purple button with the cross. This will open a list of virtual components that we can wire to the virtual Pi Pico:

[#img-virtual-component]
.Virtual Component Menu
image::./images/ledmenu.png["Component Menu",450,450]

Click on LED and a red one should appear on the screen next to the Pi Pico:

[#img-virtual-component-led]
.Red LED added to the Simulation pane
image::./images/led_display.png["Red LED in Simulator",450,450]

We now need to add a resistor. This is required to restrict the amount of current being supplied to the LED and to protect it. Select a resistor from the component menu:

[#img-virtual-component-led]
.Red LED with resistor in the Simulation pane
image::./images/led_resistor.png["Red LED and Resistor in Simulator",450,450]

[NOTE]
====

You can explore why a resistor is requried https://electronicsclub.info/leds.htm[here]
====


[TIP]
====
You can rotate components by pressing the R key
====


Next we need to wire the LED and resistor to the Pi Pico:

 * Connect the bottom of the resistor (Nearest the yellow band in the image below) to GND.4
 * Connect the top of the resistor to the straight leg of the LED.

Then connect the other leg of the LED to GPIO13.

<<<

Your wired circuit should now look like this:

[#img-virtual-component-led]
.Components wired together
image::./images/led_resistor_wired.png["Components Wired Together",450,450]


[NOTE]
====
If you hover over the pins/legs you will be shown the relevant name. 

Also, by holding down the ctrl key whilst you click the mouse button you can bend the wire.
====

At the moment the value resistor is incorrect and we need to change it to 330&#937;. We could also change the wire colours to make the diagram a bit clearer.

<<<

Click on the _diagram.json_ tab. This will open the configuration file for the project:

[#img-virtual-component-json]
.Editing the JSON config file
image::./images/diagram.json.png["JSON Configuration File",450,450]

Find the section for the resistor. In this example it starts on line 21. The last section, _"attrs"_ is where we can set the value of the resistor by updating the value. In this example we would change 220 to 330.


[source, json]
----
{ "value": "330"}
----


[#img-virtual-component-json]
.Editing the JSON config file for Resistor value
image::./images/json_resistor.png["Resistor JSON Configuration File",450,450]

Next the wire colours can be updated. 

****
Challenge:

Can you get the wires on your version to match the following:

[#img-virtual-component-wire]
.Editing the wire colour
image::./images/wires_coloured.png["Updating the Wires",300,300]

[TIP]
====

What happens if you just click on the wires?

Explore the diagram.json file a bit further

Try clicking the Docs tab and read the the section on diagram.json
====

****
<<<
Once finished it's time to get it flashing. We can use the code from before:


[source,python,linenums]
------
import machine
import utime
led_onboard = machine.Pin(25, machine.Pin.OUT)
while True:
    led_onboard.value(1)
    utime.sleep(5)
    led_onboard.value(0)
    utime.sleep(5)
------

****
Challenge:

If we click the run button then nothing will happen!

Can you think why?

Which pin is our Red LED annode (the angled leg) connected to?

Is it the same as the as the pin that is mentioned in the code above?

Can you fix the code by making a change to the code to use this pin number? 

****

Once done, click the green run button and light up your LED:

[#img-virtual-component-led]
.LED Glowing
image::./images/ledon.png["LED glowing",300,300]

****
Challenge:

Is it possible to control more than one LED?

Think about unused pins on the Pi Pico

Try adding another LED to the Pi Pico.

* Can you control both individually or both at the same time.
* Can you add another LED and control

_Don't forget that each LED connection will need it's own unique name in the code_

****

=== Final Challenge

Using the knowledge we have explored so far....can you buid a simple traffic light system?

[#img-virtual-component-led]
image::./images/traffic_light.jpg["Traffic Light",300,300]

